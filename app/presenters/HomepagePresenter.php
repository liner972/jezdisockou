<?php

namespace App\Presenters;

use App\Forms;
use Nette;
use Nette\Application\UI\Form;
use App\Model;
use Tracy\Debugger;

use App\Component\BiletoApi;


class HomepagePresenter extends BasePresenter
{


	/** @var Forms\SearchFormFactory @inject */
	public $searchFormFactory;
	
	/** @var  BiletoApi @inject */
    public $biletoApi;

	/** @var  Model\GoogleMapsModel @inject */
	public $googleApi;

	/** @var  Model\BingMapsModel @inject */
	public $bingApi;

	public function actionDefault()
	{
		if(!isset($_SESSION['fuelCost'])) {
			$_SESSION['fuelCost'] = 28;
		}
		if(!isset($_SESSION['spotreba'])) {
			$_SESSION['spotreba'] = 7.5;
		}
		if(!isset($_SESSION['passengers'])) {
			$_SESSION['passengers'] = 1;
		}
		if(!isset($_SESSION['salary'])) {
		$_SESSION['salary'] = 500;
		}
	}

	public function renderDefault($results = array())
	{
		$this->template->results = false;

		if($results) {
			$this->template->results = true;
			$_SESSION['fuelCost'] = $results['fuelCost'];
			$_SESSION['spotreba'] =  $results['spotreba'];
			$_SESSION['passengers'] =  $results['passengers'];
			$_SESSION['salary'] =  $results['salary'];
			$this->template->from = $results['from'];
			$this->template->destination = $results['destination'];


			$route = $this->bingApi->getDistance(array($results['from'], $results['destination']));
			if ($route) {
				$route['duration'] = gmdate('H:i', $route['duration']);
				$route['title'] = 'Auto';
				$route['cost'] = $route['distance']*$results['fuelCost']/100*$results['spotreba']/$results['passengers'];
				
				$data[] = $route;
			}
			/*$route = $this->googleApi->getVzdalenost($results['from'], $results['destination']);
			if ($route) {
				$route['duration'] = gmdate('H:i', $route['duration']);
				$route['title'] = 'Google';
				$route['cost'] = $route['distance']/1000*$results['fuelCost']/100*$results['spotreba']/$results['passengers'];
				$data[] = $route;
			}*/

			$date = new \DateTime();
			$date->createFromFormat('Y-m-d H:i', $results['date'].' '.$results['time']);
			$formatedDate = $date->format(\DateTime::ISO8601);

			$cityTable = $this->createTownList();
			if (array_key_exists($results['from'], $cityTable) AND array_key_exists($results['destination'], $cityTable)) {
				$route = $this->biletoApi->getReturn($cityTable[$results['from']], $cityTable[$results['destination']], $formatedDate);
			} else {
				$route = false;
			}
			if ($route) {
				$new['title'] = 'Hromadná doprava';
				$new['cost'] = $route->cena;
				$new['duration'] = $route->cas;
				$new['distance'] = 0;
				$new['route'] = array();

				foreach ($route->legs as $trat) {
					$train = $trat->type == 'rail';
					foreach ($trat->stations as $stanice) {
						if (!$train) { // TODO
							$locs[] = array($stanice->lat, $stanice->lng);
						} else {
							$new['route'][] = array($stanice->lat, $stanice->lng);
						}
					}
					if (!$train) {
						$part = $this->bingApi->getDistance($locs);
						$new['distance'] += $part['distance'];
						$new['route'] = array_merge($new['route'], $part['route']);
					} else {
						$new['distance'] += $this->distance($trat->from->lat, $trat->from->lng, $trat->to->lat, $trat->to->lng, 'K') * 1000;
					}
				}
				$new['distance'] = $new['distance'] / 1000;
				$data[] = $new;
			}

			$this->template->nemamNic = false;
			if (!isset($data)){
				$this->template->nemamNic = true;
			$this->template->data = [];
			} else {
				usort($data, array($this, "sortRoutes"));
				
				
				
				if (!isset($data[1])) {
					$data[1]['title'] = '';
					$data[1]['cost'] = 0;
					$data[1]['duration'] = 0;
					$data[1]['distance'] = 0;
					$data[1]['route'] = array();
					$data[0]['percent_cost'] = 100;
					$data[0]['percent_distance'] = 100;
					$data[0]['percent_duration'] = 100;
				} else {
					$data[0]['percent_cost'] = round((100 - (($data[0]['cost']*100) / ($data[1]['cost']))));
					$data[0]['percent_distance'] = round((100 - (($data[0]['distance']*100) / ($data[1]['distance']))));
					$duration[0] = explode(':', $data[0]['duration']);
					$duration[0] = ($duration[0][0] * 60) + $duration[0][1];
					$duration[1] = explode(':', $data[1]['duration']);
					$duration[1] = ($duration[1][0] * 60) + $duration[1][1];
					if ($duration[0] < $duration[1]) {
						$data[0]['percent_duration'] = round((100 - ((($duration[0])*100) / (($duration[1])))));
					} else {
						$data[0]['percent_duration'] = round((100 - ((($duration[1])*100) / (($duration[0])))));
					}
					if ($data[0]['percent_distance'] < 0) {
						$data[0]['percent_distance'] = $data[0]['percent_distance'] * -1;
					}
					
					if ($data[0]['percent_duration'] < 0) {
						$data[0]['percent_duration'] = $data[0]['percent_duration'] * -1;
					}
				}
				
				$data[0]['cost'] = (round($data[0]['cost']));
				$data[1]['cost'] = (round($data[1]['cost']));
				$data[0]['distance'] = round($data[0]['distance'], 2);
				$data[1]['distance'] = round($data[1]['distance'], 2);
				
				$this->template->data = $data;
				}
		}

	}

	function distance($lat1, $lon1, $lat2, $lon2, $unit) {

		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return $miles;
		}
	}

	private function sortRoutes($a, $b)
	{
		if($a['cost'] == $b['cost'])
			return 0;

		return ($a['cost'] < $b['cost']) ? -1 : 1;
	}

	/**
	 * Search form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSearchForm()
	{
		return $this->searchFormFactory->create(function () {
			$this->redirect('Homepage:' );
		});
	}


	public function createComponentSearch()
	{
		$form = new Form();
		$form->addText('from', 'Odkud:');
//			->setRequired('Odkud chcete jet?');

		$form->addText('destination', 'Kam:');
//			->setRequired('Kam chcete jet?');

		$form->addText('date', 'Datum');
//			->setType('date');

		$form->addText('time', 'Čas');
//			->setType('time');

		$form->addText('spotreba', 'Spotřeba')
//			->setType('number')
			->setDefaultValue($_SESSION['spotreba']);
//			->setRequired('Zadejte prosím průměrnou spotřebu');

		$form->addText('fuelCost', 'Cena paliva')
//			->setType('number')
//			->addRule(Form::RANGE, 'Cena paliva musí být kladné číslo.', array(0, null))
			->setDefaultValue($_SESSION['fuelCost']);
//			->setRequired('Kolik stojí benál?');

		$form->addText('passengers', 'Počet osob')
//			->setType('number')
//			->addRule(Form::RANGE, 'Počet osob musí být kladné číslo.', array(0, null))
			->setDefaultValue($_SESSION['passengers']);
//			->setRequired('Jedeš sám?');

		$form->addText('salary', 'Hodinový plat');
//			->setType('number')
//			->addRule(Form::RANGE, 'Hodinový plat musí být kladné číslo.', array(0, null))
//			->setDefaultValue($_SESSION['salary'])
//			->setRequired('Máš prostoje?');

		$form->addSubmit('send', 'Vyhledat');

//		$form->onValidate[] = [$this, 'validateSignInForm'];
		$form->onSuccess[] = array($this, 'searchFormSuccess');
		return $form;
	}
	
	
	
//
//public function validateSignInForm($form)
//{
//    $values = $form->getValues();
//
////   dump($values);
//	 return true;
//	 //('<br>sdg');
//}

	public function searchFormSuccess($form, $values)
	{
		
		$date = new \DateTime();
		
		$values['from'] = $values['from'];
		$values['destination'] = $values['destination'];
		$values['date'] = !empty($values['date'])? $values['date'] : $date->format('d.m.Y'); 
		$values['time'] = !empty($values['time'])? $values['time'] : $date->format('H:i'); 
		$values['fuelCost'] = !empty($values['fuelCost'])? $values['fuelCost'] : 28; 
		$values['spotreba'] = !empty($values['spotreba'])? $values['spotreba'] : 8; 
		$values['passengers'] = !empty($values['passengers'])? $values['passengers'] : 1; 
		$values['salary'] = !empty($values['salary'])? $values['salary'] : 500; 
		
//		echo "\n<br>ve form success";
//		dump($values);
//		die('<br>sdfdfdfs');
		//Debugger::dump($data);
		$this->redirect('this', ['results' => $values]);
	}

	public function createTownList()
	{
		$file = fopen("bileto_cities.csv","r");
		$list = array();
		while(! feof($file))
		{
			$line = explode(';', fgetcsv($file)[0]);
			$list[$line[1]] = $line[0];
		}

		fclose($file);
		return $list;
	}
}
